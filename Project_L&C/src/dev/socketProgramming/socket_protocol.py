import socket


class SocketProtocol():

    def host(self):
        return socket.gethostname()

    def port(self):
        return 8080

    def socket_instance(self):
        return socket.socket()
