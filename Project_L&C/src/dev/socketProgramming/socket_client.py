import socket
from socket_protocol import SocketProtocol


class SocketClient(SocketProtocol):

    # creating an instance of SocketProtocol class
    socket_protocol = SocketProtocol()

    def client(self):

        global host, port, client_socket

        host = self.socket_protocol.host()
        port = self.socket_protocol.port()
        client_socket = self.socket_protocol.socket_instance()

        self.connect_socket(host=host, port=port)

    def connect_socket(self, host, port):

        print('Connection established...!')

        try:
            client_socket.connect((host, port))
        except socket.error as e:
            print(str(e))

        self.response_data_from_server()

    def response_data_from_server(self):
        try:
            response = client_socket.recv(1024)
        except socket.error as e:
            print(str(e))

        while True:
            request = input('Request from Client: ')
            client_socket.send(str.encode(request))
            response = client_socket.recv(1024)
            print(response.decode('utf-8'))

        client_socket.close()


if __name__ == '__main__':
    client = SocketClient()
    client.client()
