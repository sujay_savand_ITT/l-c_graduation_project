import socket
import _thread
import time
import datetime
from socket_protocol import SocketProtocol
from load_response_files import ResponseFiles
from socket_db import DataBase


class SocketServer(SocketProtocol):

    # creating an instance of SocketProtocol class
    socket_protocol = SocketProtocol()

    # creating an instance of ResponseFiles class
    response_data = ResponseFiles()

    # creating an instance of DataBase class
    db = DataBase()
    thread_count = 0

    def server(self):

        global host, port, server_socket

        host = self.socket_protocol.host()
        port = self.socket_protocol.port()
        server_socket = self.socket_protocol.socket_instance()
        self.socket_bind(host=host, port=port)

    def socket_bind(self, host, port):

        try:
            server_socket.bind((host, port))
        except socket.error as e:
            print(str(e))
        print('Waiting for a Connection..')
        server_socket.listen(5)

    def server_accept(self):

        while True:
            global client_name
            client, address = server_socket.accept()
            client_name = address[0] + ':' + str(address[1])
            print('Connected to: ', client_name)
            _thread.start_new_thread(
                self.threaded_client_communication, (client, address))

            self.thread_count += 1
            print('Client Number: ' + str(self.thread_count))

        server_socket.close()

    def threaded_client_communication(self, connection, address):

        connection.send(str.encode('Welcome to the Server\n'))
        self.response_data.create_output_directory('output')
        self.db.create_client_table(client_name)
        self.db.insert_into_reference_table(client_name)

        while True:
            data = connection.recv(2048)
            response = data.decode('utf-8')
            if not data:
                break
            connection.sendall(str.encode(response))

            connection_time = self.time_stamp()
            self.db.insert_into_client_table(client_name, response)
            self.response_data.write_response_data_to_file(
                'output', address, connection_time, response)

        connection.close()

    def time_stamp(self):

        timestamp = time.time()
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d::%H:%M:%S - ')


if __name__ == '__main__':

    server = SocketServer()
    server.server()
    server.server_accept()
