import mysql.connector


class DataBase:
    database_name = 'IMQ'

    def __init__(self):
        self.connect()
        self.create_database()
        self.use_database(self.database_name)
        self.create_clients_reference_table()

    def connect(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="sujay",
        )

    def create_database(self):
        self.mycursor = self.mydb.cursor()
        query = "CREATE DATABASE IF NOT EXISTS "+self.database_name
        self.execute(query)

    def use_database(self, database_name):
        query = 'USE '+database_name
        self.execute(query)

    def create_clients_reference_table(self):
        query = 'CREATE TABLE IF NOT EXISTS reference (id INT AUTO_INCREMENT PRIMARY KEY, client_name VARCHAR(255) NOT NULL UNIQUE)'
        self.execute(query)

    def create_client_table(self, table_name):
        query = 'CREATE TABLE IF NOT EXISTS `'+table_name + \
            '` (`id` INT AUTO_INCREMENT PRIMARY KEY, `received_time` datetime, data VARCHAR(255))'
        self.execute(query)

    def insert_into_reference_table(self, client_name):
        query = "INSERT into `reference` (`client_name`) values('" + \
            client_name+"')"
        self.execute(query)

    def insert_into_client_table(self, client_name, data):
        query = "INSERT into `"+client_name + \
            "`(`received_time`,`data`) values( NOW(),'"+data+"')"
        self.execute(query)

    def execute(self, query):
        try:
            self.mydb.start_transaction()
            self.mycursor.execute(query)
            self.mydb.commit()
        except mysql.connector.Error as err:
            print("Something went wrong while executing the query: {}".format(err))
